(function (root, ns, factory) {
    'use strict';
    if (typeof define === 'function' && define.amd) {
        define(factory);
    } else {
        root.ttlStorage = factory();
    }
} (window, 'ttlStorage', function() {
    /************************************
     * private variables
     ************************************/
    var prefix = 'ts_',
        supported = (('localStorage' in window) && window.localStorage !== null) ? true : false;

    /************************************
     * private methods
     ************************************/
    function clearKey(key){
        var fullKey = getFullKey(key),
            result = false;

        if (supported) {
            result = window.localStorage.removeItem(fullKey);
        }
        return result;
    }

    function getFullKey(key){
        return prefix + key;
    }

    /************************************
     * public methods
     ************************************/
    return {
        set: function(key, value, ttl){
            if (!supported) {
                return false;
            }

            try {
                value = JSON.parse(JSON.stringify(value));
            } catch (E) {
                return E;
            }

            var curtime = +new Date(), // convert date to number
                expire = curtime + ttl, // set expiration time
                // set up the object for storage
                obj = {
                    'value':value,
                    'expire': expire
                },
                fullKey = getFullKey(key);

            try {
                localStorage.setItem(fullKey, JSON.stringify(obj));
            } catch (E) {
                return E;
            }
            return true;
        },
        get: function(key){
            if (!supported) {
                return false;
            }
            var curtime = +new Date(),
                fullKey = getFullKey(key);
            
            // grab it
            var item = (!((window.localStorage.getItem(fullKey)  === null) || (window.localStorage.getItem(fullKey ) === 'undefined'))) ? JSON.parse(window.localStorage.getItem(fullKey)) : false;
            
            // if item is there but expired
            if((item) && (item.expire <= curtime)){
                // clear item from storage
                clearKey(key);
                // set return
                item = false;
            }

            // return just the value if it's present
            return (item.hasOwnProperty('value')) ? item.value : item;
        },
        delete: function(key){
            return clearKey(key);
        }
    };
}));