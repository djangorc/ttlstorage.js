ttlStorage.js
========================
Extremely basic javascript wrapper module for working with local storage in a way that allows for object expiration.

---

## TODO

* right now expired objects are only delete if something tries to access them again.  this could lead to expired objects living in local storage basically forever.
