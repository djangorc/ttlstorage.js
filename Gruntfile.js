module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    uglify: {
      options: {
        banner: '/*! \n * <%= pkg.name %> v<%= pkg.version %> (<%= grunt.template.today("yyyy-mm-dd") %>)\n * Licensed under MIT\n */\n'
      },
      build: {
        src: 'src/ttlStorage.js',
        dest: 'dist/ttlStorage.min.js'
      }
    },
    watch: {
        files: [
            'src/**/*.js'
        ],
        tasks : ['uglify']
    }
  });

  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('default', ['watch']);
};